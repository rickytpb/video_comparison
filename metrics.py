from __future__ import division
from skimage.measure import compare_nrmse
from scipy.misc import imresize
import time
import numpy as np
from scipy.ndimage import uniform_filter, gaussian_filter
import multiprocessing
from skimage.util.dtype import dtype_range
from skimage.util.arraycrop import crop
from skimage._shared.utils import deprecated
from skimage._shared.utils import skimage_deprecation, warn


class StatMeasure(object):
    def __init__(self):
        super().__init__()
        self.__pool = multiprocessing.Pool(3)

    def __del__(self):
        self.__pool.close()

    def compare_frames(self, imageA, imageB, multi=True):
        m = self.get_mse(imageA, imageB)
        s = self.get_ssim(imageA, imageB, multi)
        return [m, s]

    def get_mse(self, imageA, imageB):
        if imageA.shape != imageB.shape:
            imageB = imresize(imageB, imageA.shape)
        mse = compare_nrmse(imageA, imageB)
        return 1 - mse

    def get_ssim(self, imageA, imageB, multi):
        if imageA.shape != imageB.shape:
            imageB = imresize(imageB, imageA.shape)
        return self.compare_mssim(imageA, imageB, 7)
        # return compare_ssim(imageA,imageB,win_size=7,multichannel=multi)

    def compare_mssim(self, X, Y, win_size=None):
        if not X.dtype == Y.dtype:
            raise ValueError('Input images must have the same dtype.')

        if not X.shape == Y.shape:
            raise ValueError('Input images must have the same dimensions.')

        # loop over channels
        nch = X.shape[-1]
        mssim = np.empty(nch)
        # for ch in range(nch):
        #    ch_result = compare_ssim(X[..., ch], Y[..., ch], **args)
        #    mssim[..., ch] = ch_result

        params = []
        for ch in range(nch):
            x = (X[..., ch])
            y = (Y[..., ch])
            params.append([x, y, win_size])
        results = self.__pool.map(self.compare_ssim, params)
        mssim = np.mean(results)
        return mssim

    @staticmethod
    def compare_ssim(data):
        X = data[0]
        Y = data[1]
        win_size = data[2]
        if not X.dtype == Y.dtype:
            raise ValueError('Input images must have the same dtype.')

        if not X.shape == Y.shape:
            raise ValueError('Input images must have the same dimensions.')

        K1 = 0.01
        K2 = 0.03
        sigma = 1.5
        if K1 < 0:
            raise ValueError("K1 must be positive")
        if K2 < 0:
            raise ValueError("K2 must be positive")
        if sigma < 0:
            raise ValueError("sigma must be positive")
        use_sample_covariance = True

        if win_size is None:
            win_size = 7  # backwards compatibility

        if np.any((np.asarray(X.shape) - win_size) < 0):
            raise ValueError(
                "win_size exceeds image extent.  If the input is a multichannel "
                "(color) image, set multichannel=True.")

        if not (win_size % 2 == 1):
            raise ValueError('Window size must be odd.')

        dmin, dmax = dtype_range[X.dtype.type]
        data_range = dmax - dmin

        ndim = X.ndim

        filter_func = uniform_filter
        filter_args = {'size': win_size, 'mode': 'nearest'}

        # ndimage filters need floating point data
        X = X.astype(np.float64)
        Y = Y.astype(np.float64)

        NP = win_size ** ndim

        # filter has already normalized by NP
        if use_sample_covariance:
            cov_norm = NP / (NP - 1)  # sample covariance
        else:
            cov_norm = 1.0  # population covariance to match Wang et. al. 2004

        # compute (weighted) means
        ux = filter_func(X, **filter_args)
        uy = filter_func(Y, **filter_args)

        # compute (weighted) variances and covariances
        uxx = filter_func(X * X, **filter_args)
        uyy = filter_func(Y * Y, **filter_args)
        uxy = filter_func(X * Y, **filter_args)
        vx = cov_norm * (uxx - ux * ux)
        vy = cov_norm * (uyy - uy * uy)
        vxy = cov_norm * (uxy - ux * uy)

        R = data_range
        C1 = (K1 * R) ** 2
        C2 = (K2 * R) ** 2

        A1, A2, B1, B2 = ((2 * ux * uy + C1,
                           2 * vxy + C2,
                           ux ** 2 + uy ** 2 + C1,
                           vx + vy + C2))
        D = B1 * B2
        S = (A1 * A2) / D

        # to avoid edge effects will ignore filter radius strip around edges
        pad = (win_size - 1) // 2

        # compute (weighted) mean of ssim
        mssim = crop(S, pad).mean()

        return mssim


class TimeConsume(object):
    def __init__(self):
        super().__init__()
        self.__results = []
        self.__timer = 0

    def start(self):
        self.__timer = time.time()

    def end(self):
        self.__timer = abs(self.__timer - time.time())
        self.__results.append(self.__timer)

    def get_results(self):
        return self.__results

    def save_results_to_file(self, fname):
        with open(fname, "w+") as file:
            for item in self.__results:
                file.write(str(item) + '\n')
