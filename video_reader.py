import cv2
import numpy as np
import pickle
import time
import sys

class VideoReader(object):
    def __init__(self):
        super().__init__()
        self.__data = dict()
        self.__counter = 0

    def read_to_tree(self, src, downsample_ratio=1):
        cap = cv2.VideoCapture(src)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH,1280)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT,720)
        cap.set(cv2.CAP_PROP_FPS,30)
        while cap.isOpened():
            ret, frame = cap.read()
            if ret is True:
                frame = cv2.resize(frame, None, fx=downsample_ratio, fy=downsample_ratio)
                self.__data[self.__counter] = frame
                self.__counter += 1
            else:
                break


    def get_data(self):
        return self.__data

    def save_data(self, filename):
        f = open(filename,'wb')
        pickle.dump(self.__data, f)
        f.close()

    def reset(self):
        self.__data = dict()
        self.__counter = 0


if __name__ == "__main__":
    vr = VideoReader()
    # ir = VideoReader()
    vr.read_to_tree("BigBuckBunny.mp4")
    # ir.read_to_tree("test_file.avi")
    x = vr.get_data()
    vr.save_data('test.dat')
    # y = ir.get_data()
    # print(max([len(x),len(y)]))
