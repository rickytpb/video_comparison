from video_comparator import VideoComparator
import os
import sys

def get_other_videos(path, source):
    vids = []
    for item in os.listdir(path):
        if item != source:
            vids.append(item)
    return vids

def save_results_to_file(name, results):
    fname = name.split('.')[0] + '.txt'
    fname = os.path.join('results',fname)
    with open(fname,"w+") as file:
        for item in results:
            sys.stdout.flush()
            for metric in item:
                file.write(str(metric)+ " ")
            file.write('\n')

if __name__ == "__main__":
    import time
    path = "videos"
    source = "source.mp4"
    for vid in get_other_videos(path,source):
        vc = VideoComparator(os.path.join(path,source),os.path.join(path,vid), True)
        start = time.time()
        res = vc.compare_videos(greedy=True,downsample_ratio=0.4)
        end = time.time()
        print(end-start)
        save_results_to_file(vid,res)

