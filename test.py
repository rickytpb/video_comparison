from scipy.stats import f_oneway, shapiro, kruskal, bartlett
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
data = pd.read_csv('~/chrille_data_2.csv')
x = data.iloc[:,0].values
y = data.iloc[:,1].values

steps = np.arange(0,len(x),1) + 1
#plt.scatter(steps, x,color='red')
#plt.scatter(steps, y, color='yellow')
#plt.show()

p_value = shapiro(x)[1]
print(p_value)
print(bartlett(x,y))
if(p_value<0.05):
    print(kruskal(x,y))
else:
    print(f_oneway(x,y))