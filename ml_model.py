from sklearn.ensemble import RandomForestClassifier


class Classifier(object):

    def __init__(self):
        super().__init__()
        self._classifier = None




class FrameClassifier(Classifier):
    def __init__(self):
        super().__init__()
        self._classifier = RandomForestClassifier()

    def fit(self, x, y):
        self._classifier.fit(x, y)

    def predict(self, x):
        return self._classifier.predict(x)


class VideoClassifier(Classifier):
    def __init__(self):
        super().__init__()
        self._classifier = RandomForestClassifier()

    def fit(self, x, y):
        self._classifier.fit(x, y)

    def predict(self, x):
        return self._classifier.predict(x)
