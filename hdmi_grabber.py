import cv2
import sys
import signal
import argparse
import os
import stat

class VideoReader(object):

    def __init__(self, fname, codec):
        super().__init__()
        self.__filename = fname
        self.__codec = codec
        self.__run = True

    def __sig_handler(self, signum, frame):
        print("I've catched a signal %i - I'm stopping grabbing HDMI" % signum)
        self.__run = False

    @staticmethod
    def __validate_capture_device(device):
        if os.path.exists(device):
            if not stat.S_ISCHR(os.stat(device).st_mode):
                print("Not a character device under this path %s" % device, file=sys.stderr)
                sys.exit(1)
        else:
            print("No device under this file %s" % device, file=sys.stderr)
            sys.exit(1)

    @staticmethod
    def __validate_dimensions(res):
        if res[0]>3840 or res[0]<1:
            print("Invalid resolution, width specified - %d" % res[0], file=sys.stderr)
            print("Should be between 1 and 3840", file=sys.stderr)
            sys.exit(1)
        if res[1]>2160 or res[1]<1:
            print("Invalid resolution, height specified - %d" % res[1], file=sys.stderr)
            print("Should be between 1 and 2160", file=sys.stderr)
            sys.exit(1)
        if res[2] and res[2]<1:
            print("Invalid FPS number - %d! Can't be negative" % res[2], file=sys.stderr)
            sys.exit(1)

    def grab_hdmi_output(self, device, dimensions, offset=None, time=None):
        self.__validate_capture_device(device)
        self.__validate_dimensions(dimensions)
        print("To end recording you can send SIGINT"
              "[CTRL]+[C] or kill -2 %d" % os.getpid())
        signal.signal(signal.SIGINT, self.__sig_handler)
        width = dimensions[0]
        height = dimensions[1]
        fps = dimensions[2]
        reader = self.__get_reader(device, width, height, fps)

        if offset:
            try:
                width_border, height_border = self.__get_cropped_size(width, height, offset)
                width = width - sum(width_border)
                height = height - sum(height_border)
            except AttributeError:
                print("Wrong offset - video is %dx%d" % (width,height), file=sys.stderr)
                print("You're offset is", *offset, file=sys.stderr)
                print("Recording video fullsize", file=sys.stderr)
                offset = None

        if not fps:
            fps = cv2.CAP_PROP_FPS
        writer = self.__get_writer(self.__filename, self.__codec, width, height, fps)

        if time:
            signal.signal(signal.SIGALRM, self.__sig_handler)
            signal.alarm(time)

        while reader.isOpened():
            ret, frame = reader.read()
            if ret and self.__run:
                if offset:
                    top = height_border[0]
                    down = height_border[1]
                    left = width_border[0]
                    right = width_border[1]
                    frame=frame[top:dimensions[1]-down,left:dimensions[0]-right,:]
                writer.write(frame)
            else:
                writer.release()
                reader.release()
                break

    @staticmethod
    def __get_writer(file, codec, width, height, fps):
        fourcc = cv2.VideoWriter_fourcc(*codec)
        return cv2.VideoWriter(file, fourcc, fps, (width, height))

    @staticmethod
    def __get_reader(device, width, height, fps):
        cap = cv2.VideoCapture(device)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        if fps:
            cap.set(cv2.CAP_PROP_FPS, fps)
        return cap

    @staticmethod
    def __get_cropped_size(width, height, offset):
        top = offset[0]
        right = offset[1]
        down = offset[2]
        left = offset[3]
        if (top + down) >= height:
            raise AttributeError
        if (left + right) >= width:
            raise AttributeError
        return [left, right], [top, down]


    @staticmethod
    def res_off_to_list(argument):
        argument = str(argument).lower().strip()
        options = [int(opt) for opt in argument.split('x')]
        return options



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="This script grabs video from a device (like HDMI grabber/webcam)"
                    "You can set resolution and fps in which video should be grabbed and also"
                    "crop the video to custom size. It uses ffmpeg and opencv."
                    "To end recording you can use --time parameter or you can send SIGINT")
    parser.add_argument('device',
                        help="Device from which video will be recorder")
    parser.add_argument('--outfile',
                        help="File to which video will be saved",
                        default="outfile.mkv")
    parser.add_argument('--codec',
                        help="Codec which will be used, note that ffmpeg checks if codec is compatible"
                             "with file extension",
                        default="H264")
    parser.add_argument('--resolution',
                        help="Resolution (in format [width]x[height]) in which video will be grabbed",
                        default="1280x720")
    parser.add_argument('--fps',
                        help="Number of frames per second that will be recorded"
                             "WARNING - this option sometimes works not as intended",
                        default=0)
    parser.add_argument('--offset',
                        help="Offset from the original resolution, video will be cropped by provided"
                             "margins. Margins should be in this format [top]x[right]x[down]x[left]",
                        default=None)
    parser.add_argument('--time',
                        help="Time after recording will be stopped",
                        default=None, type=int)
    args = parser.parse_args()

    try:
        dimensions = VideoReader.res_off_to_list(args.resolution)
        dimensions.append(int(args.fps))
        vr = VideoReader(args.outfile, args.codec)
    except ValueError as e:
        print("Wrong argument value, please read help", file=sys.stderr)
        print(e,file=sys.stderr)
        sys.exit(1)

    if args.offset:
        args.offset = VideoReader.res_off_to_list(args.offset)
    vr.grab_hdmi_output(args.device,dimensions=dimensions, time=args.time, offset=args.offset)
