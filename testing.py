from matplotlib import pyplot as plt
import pandas as pd
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from scipy.stats import test
from statsmodels.stats.weightstats import ztest
import numpy as np
import os
import sys


def var_mean_min_max(data):
    mean = np.mean(data)
    min = np.amin(data)
    maxi = np.amax(data)
    var = np.var(data)
    arr = np.array([mean, maxi - min])
    return arr


def compose_line(mse_data, ssim_data, moves):
    mse = var_mean_min_max(mse_data)
    ssim = var_mean_min_max(ssim_data)
    sum_moves = np.sum(moves)
    arr = np.append(ssim, mse)
    arr = np.append(arr,sum_moves)
    return arr


good_movies = ['black_ending_longer.txt', 'start_delay_normal.txt', 'lossless.txt', 'hd_video.txt', 'h264high.txt',
               'codec_hdv.txt']


def get_results():
    src = pd.read_csv('results/source_same.txt', sep=' ')
    src = src.values[:, 0:3]
    l = compose_line(src[:, 0], src[:, 1], src[:,2])
    l = np.append(l, [1,'source_same.txt'])
    results = l
    for file in os.listdir('./results'):
        if file.endswith('txt') and file not in ['source_same.txt', 'compare.txt', 'fit.txt']:
            path = os.path.join('results', file)
            dst = pd.read_csv(path, sep=' ')
            dst = dst.values[:, 0:3]
            print(file)
            l = compose_line(dst[:, 0], dst[:, 1], dst[:,2])
            if file in good_movies:
                l = np.append(l, [1,file])
            else:
                l = np.append(l, [0,file])
            print(l)
            results = np.vstack((results, l))

    return results


res = get_results()
res = np.array(res)
for result in res:
    if result[-2] == '1':
        print(result)
        plt.scatter(float(result[2]),float(result[4]),color="red", label=result[-1])
    else:
        print(result)
        plt.scatter(float(result[2]),float(result[4]),color="blue", label=result[-1])





overall_score = 0
x = 1
for i in range(x):
    X_train, X_test, y_train, y_test = train_test_split(
        res[:, :-1], res[:, -1], test_size=0.1, shuffle=True, stratify=res[:,-1])
    clf = AdaBoostClassifier(random_state=1)
    clf.fit(X_train, y_train)
    preds = clf.predict((X_test))
    score = clf.score(X_test, y_test)
    overall_score += score
    class_names = ['BAD', 'GOOD']
    f_names = ['s mean', 's var', 's range']

    # viz = export_graphviz(clf,out_file=None,class_names=class_names,feature_names=f_names)
    # graph = graphviz.Source(viz)
    # graph.render('./graph.gv',view=True)
print(overall_score / x)
