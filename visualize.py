import matplotlib.pyplot as plt

def show_chart():
    plt.scatter(list(),list(), color='red')
    plt.scatter(list(),list(), color='blue')
    plt.xlabel('Time')
    plt.ylabel('SSIM/MSE')
    plt.ylim(0,1)
    plt.show()