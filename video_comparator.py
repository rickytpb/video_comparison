# import the necessary packages
from video_reader import VideoReader
from metrics import StatMeasure
from matplotlib import pyplot as plt
import numpy as np
from metrics import TimeConsume
import pickle

class VideoComparator(object):
    def __init__(self, src_vid, dst_vid, debug=False):
        super().__init__()
        self.__source = src_vid
        self.__dest = dst_vid
        self.__vr = VideoReader()
        self.__ssim_threshold = 0.9
        self.__mse_threshold = 0.9
        self.__offset = 0
        self.__prev = -1
        self.__measure = StatMeasure()
        self.__tc_compare = None
        self.__tc_fit = None
        if debug:
            self.__tc_compare = TimeConsume()
            self.__tc_fit = TimeConsume()

    def compare_videos(self, greedy=False, downsample_ratio=1):
        print("Reading videos")
        self.__vr.read_to_tree(self.__source, downsample_ratio)
        src_data = self.__vr.get_data()
        self.__vr.reset()
        self.__vr.read_to_tree(self.__dest, downsample_ratio)
        dst_data = self.__vr.get_data()
        start = self.__find_start(src_data, dst_data)
        print("Starting getting results")
        results = self.__get_mse_ssim_match_frame(src_data, dst_data, start, greedy)
        if self.__tc_compare and self.__tc_fit:
            self.__tc_compare.save_results_to_file("compare.txt")
            self.__tc_fit.save_results_to_file("fit.txt")
        return results

    def __get_mse_ssim_match_frame(self, src_data, dst_data, start, greedy, moved=0):
        results = []
        for i in range(len(src_data)):
            print("Frame no %d" % i)
            try:
                if self.__tc_compare:
                    self.__tc_compare.start()
                if moved:
                    index = i+start+self.__offset
                else:
                    index = i
                mse, ssim = self.__measure.compare_frames(src_data[index], dst_data[i+start])
                if self.__tc_compare:
                    self.__tc_compare.end()
                if greedy:
                    if mse < self.__mse_threshold and ssim < self.__ssim_threshold:
                        new_index = self.__try_fitting(src_data, dst_data, i, i + start, mse, ssim)
                        if new_index != i:
                            self.__offset = i-new_index
                            results.extend(
                                self.__get_mse_ssim_match_frame(
                                    src_data, dst_data, new_index, False, 1)
                            )
                            break
                results.append([mse, ssim, moved])
                moved = 0
            except KeyError:
                print("Tested video is too short compared to the source")
                results.append([0, 0, 0])
        return results

    def __try_fitting(self, src, dst, src_i, dst_i, oldmse, oldssim, window=5):
        print("I'm trying to fit")
        if self.__tc_fit:
            self.__tc_fit.start()
        src_keys = self.__get_slice_in_window(src, src_i, window)
        print(src_keys)
        for i in src_keys:
            mse = self.__measure.get_mse(src[i], dst[dst_i])
            if mse>(oldmse+0.1):
                ssim = self.__measure.get_ssim(src[i],dst[dst_i],True)
                if ssim > (oldssim+0.1):
                    print("CHANGED FRAME to %d!\n" % i)
                    return i
        if self.__tc_fit:
            self.__tc_fit.end()
        return src_i

    @staticmethod
    def __get_slice_in_window(data, start, window):
        l, r = (start - window, start + window)
        if l < 0:
            l = 0
        if r > len(data):
            r = len(data)
        return list([l,r])

    def __find_start(self, src_data, dst_data, part=2):
        if len(src_data) > len(dst_data):
            length = len(dst_data)
        else:
            length = len(src_data)
        start_point = 0
        for i in range(round(length/part)):
            mse, ssim = self.__measure.compare_frames(src_data[0], dst_data[i])
            print(mse,ssim)
            if mse > self.__mse_threshold and ssim > self.__ssim_threshold:
                start_point = i
                break
            else:
                prev_mse = mse
                prev_ssim = ssim
                if mse > prev_mse and prev_ssim > ssim:
                    start_point = i
        return start_point

    @staticmethod
    def show_chart(results):
        data = np.array(results)
        plt.scatter(np.arange(len(results)), data[:, 1], c="blue", alpha=0.5)
        plt.scatter(np.arange(len(results)), data[:, 0], c="red", alpha=0.5)
        plt.show()


if __name__ == "__main__":
    vc = VideoComparator("small.mp4", "outfile.mkv", True)
    res = vc.compare_videos(greedy=True,downsample_ratio=1)
    VideoComparator.show_chart(res)
